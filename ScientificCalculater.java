package inheritance;

public class ScientificCalculater {

	public double power(int base, int exponent) {
		return Math.pow(base, exponent);
	}

	public double squareRoot(int number) {
		return Math.sqrt(number);
	}

}
